import csv
import os
from kivy.app import App
from kivy.lang import Builder
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.scrollview import ScrollView
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.properties import ObjectProperty, ListProperty

class Classroom:
    'Information in Programming Fundamental Classroom'
    amount_student = 0
    list_student = []
    amount_check = 0
    list_checked = []
    list_week = []
    def findindexID(ID):
        for check in range(len(Classroom.list_student)):
            if Classroom.list_student[check].ID == ID:
                return check
        return -1

class Student:
    'creat a student info for every student'
    def __init__(self,ID,name,surname):
        self.name = name
        self.surname = surname
        self.ID = ID
        self.week_check = []
        self.count_number = []
        self.list_time = []
        Classroom.amount_student += 1
        
    def checktime(self):
        if len(week_check)<(Classroom.amount_check*80/100):
            return 1
        return 0
    
    def getpercent(self):
        return (len(self.week_check)/Classroom.amount_check)*100        

def isExists(Student, ID):
    for s in range(len(Student)):
        if Student[s].ID == ID:
            return (1, s)
    return (0, len(Student))

week = ["1-1","1-2","2-1","2-2","3-1","3-2","4-1","4-2","5-1","5-2"]
before=""
for j in range(len(week)):
    Classroom.amount_check +=1
    show=[]
    get=""
    check=[]
    checked_student=0
    name_file = "C:\\Users\\simdd\\Documents\\NetBeansProjects\\A2\\src\\Original\\Attendance w"+week[j]+" (Responses) - Form Responses 1.csv"
    with open(name_file, encoding='utf8') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        for row in readCSV:
            z = row[1].split("-",1)
            if (z[0]!="58" and z[0]!="57" and z[0]!="56" and z[0]!="55" and z[0]!="e.g.58" and z[0]!="Student ID") :
                row[1] = "58-"+z[1]
            if (row[1]=="55-010126-2015-04"): #Hard Code Check
                row[1]="55-010126-2015-4"
            if (row[2]=="Jirawut"): #Hard Code Check
                row[2]="จิรวุฒน์"
                row[3]="ท้วมเพ็ง"
            get = row[0]+","+row[1]+","+row[2]+","+row[3]+","+row[4]
            after = row[1]+row[4]
            if before == after:
                show.pop()
                check.pop()
                show.append(get)
            else:
                show.append(get)
            if row[4] == "Counted number":
                check.append(-1)
            else:
                check.append(int(row[4]))
                checked_student+=1
            if row[1] == "e.g.58-010126-1014-8": #remove e.g.(wrong type signed)
                show.pop()
                check.pop()
            before=after
            after=""
    sortstatus = 1
    while(sortstatus):
        sortstatus = 0
        for i in range(len(check)-1):
            if i == 0:continue
            elif check[len(check)-1]=="Counted number":
                sortstatus = 0
            else:
                if check[i]==0:
                    check[i]=99
                if check[i]>check[i+1] and check[i+1]!=0 :
                    temp1 = show[i]
                    show[i] = show[i+1]
                    show[i+1] = temp1
                    temp = check[i]
                    check[i] = check[i+1]
                    check[i+1] = temp
                    sortstatus = 1
                    
    count = 0
    while count < len(show)-1: #check same person who signed repeat
        word1 = show[count].split(",",1)
        word2 = show[count+1].split(",",1)
        if word1[1]==word2[1]:
            show.pop(count)
            check.pop(count)
            count-=1
        count+=1
    count = 0
    while count < len(show)-1: #check same number and remove later person
        if check[count] == check[count+1] and check[count]!=99:
            show.pop(count+1)
            check.pop(count+1)
            count-=1
        count+=1
    count = 1
    while count < len(show)-1: #for add student
        wordtmp = show[count].split(",")
        time_check = wordtmp[0]
        student_number_in = wordtmp[1].replace("-",'')
        student_name_in = wordtmp[2]
        student_surname_in = wordtmp[3]
        result = isExists(Classroom.list_student, student_number_in)
        if not(result[0]): #if not repeat student
            student1 = Student(student_number_in,student_name_in,student_surname_in)
            Classroom.list_student.append(student1)
        Classroom.list_student[result[1]].week_check.append(week[j])
        Classroom.list_student[result[1]].count_number.append(check[count])
        Classroom.list_student[result[1]].list_time.append(time_check)
        count+=1
    Classroom.list_checked.append(checked_student)
    Classroom.list_week.append(week[j])
                    
    csvfile.close()
    name_new_file = "C:\\Users\\simdd\\Documents\\NetBeansProjects\\A2\\src\\Clean\\Attendance w"+week[j]+".csv"
    with open(name_new_file, 'w') as csvwrite:
        writer = csv.writer(csvwrite, delimiter='\n' ,dialect='excel-tab')
        writer.writerow(show)
    csvwrite.close()

overdue=[]
overdue.append("Student Number,Student Name,Student Surname,Percent Checked")
for check in range(len(Classroom.list_student)):
    if Classroom.list_student[check].getpercent()<80:
        overdue.append(Classroom.list_student[check].ID+","+Classroom.list_student[check].name+","+Classroom.list_student[check].surname+","+str(Classroom.list_student[check].getpercent()))

name_overdue_file = "C:\\Users\\simdd\\Documents\\NetBeansProjects\\A2\\src\\Clean\\Overdue List.csv"
with open(name_overdue_file, 'w') as csvwrite2:
    writer1 = csv.writer(csvwrite2, delimiter='\n' ,dialect='excel-tab')
    writer1.writerow(overdue)
csvwrite.close()

    
class Register(BoxLayout):
    listname = ObjectProperty()
    student = ObjectProperty()
    listoverdue = ObjectProperty()
    input_1 = ObjectProperty()
    listamount = Classroom.list_checked
    
    def readCSV(self,file):
        self.list_all=[]
        name_file = "C:\\Users\\simdd\\Documents\\NetBeansProjects\\A2\\src\\Clean\\"+file+".csv"
        with open(name_file) as csvfile:
            readCSV = csv.reader(csvfile, delimiter=',')
            for row in readCSV:
                wordread=[]
                if row!=[]:
                    if row[0]=="Timestamp":
                        self.list_all.append(row[1]+"|"+row[2]+"|"+row[3]+"|"+row[4]+"|Percent")
                    else:
                        id_find = row[1].replace("-",'')
                        index = Classroom.findindexID(id_find)
                        self.list_all.append(row[1]+"|"+row[2]+"|"+row[3]+"|"+row[4]+"|"+str(Classroom.list_student[index].getpercent()))
        csvfile.close
        
    def showlistName(self,filename):
        self.readCSV(filename)
        self.listname.clear_widgets()
        space = GridLayout(cols=1, spacing=10, size_hint_y=None)
        space.bind(minimum_height=space.setter('height'))
        for run in self.list_all:
            extract = run.split("|")
            column = len(extract)
            layout = GridLayout(cols=column, spacing=10, size_hint_y=None)
            layout.bind(minimum_height=layout.setter('height'))
            for word in extract:
                layout.add_widget(Label(text=word, font_name='C:\\Users\\simdd\\Documents\\NetBeansProjects\\A2\\src\\t.ttf', font_size=22, size_hint_y=None, height=25))
            space.add_widget(layout)
        self.listname.add_widget(space)
        
    def showGraph(self):
        for amount in Classroom.checked_student:
            listchecked.append(amount)
        return listchecked

    def showInfo(self):
        self.studentinfo = []
        self.student.clear_widgets()
        info = GridLayout(cols=1, spacing=10, size_hint_y=None)
        info.bind(minimum_height=info.setter('height'))
        pos = Classroom.findindexID(str(self.input_1.text))
        if pos>=0:
            for run in range(len(Classroom.list_student[pos].week_check)):
                if Classroom.list_student[pos].count_number[run] == 99:
                    status = "Late"
                    self.studentinfo.append([Classroom.list_student[pos].week_check[run],status])
                else:
                    status = "Present"
                    self.studentinfo.append([Classroom.list_student[pos].week_check[run],status])
            for row in self.studentinfo:
                column = len(row)
                layout = GridLayout(cols=column, spacing=10, size_hint_y=None)
                layout.bind(minimum_height=layout.setter('height'))
                for word in row:
                    layout.add_widget(Label(text=word, font_name='C:\\Users\\simdd\\Documents\\NetBeansProjects\\A2\\src\\t.ttf', font_size=22, size_hint_y=None, height=25))
                info.add_widget(layout)
            self.student.add_widget(info)
        else:
            layout = GridLayout(cols=1, spacing=10, size_hint_y=None)
            layout.bind(minimum_height=layout.setter('height'))
            layout.add_widget(Label(text="Can not Found"))
            info.add_widget(layout)
            self.student.add_widget(info)
                
to_open = Builder.load_file("register.kv")
        
class RegisterApp(App):
    def build(self):
        return to_open

if __name__=="__main__":
    RegisterApp().run()
